\documentclass[11pt]{article}
\usepackage{amsmath,amsthm,mathptmx}
\usepackage[T1]{fontenc}
\usepackage{tikz}

\theoremstyle{definition}
\newtheorem{defn}{Definition}

\theoremstyle{remark}
\newtheorem*{note}{\textbf{\emph{Note}}}

\usetikzlibrary{positioning}

\begin{document}

\section*{Exercise 3:}

Concepts used: \emph{Graphs, simple graphs, walks, trails, paths, cycles, Null graphs,
Hypercube graphs, subgraphs, supergraphs, Bipartite graphs, connected graphs, connected
components}

\subsection*{Background and Definitions:}

Given a collection of objects $\mathit{V}$ where there may be a multiset $\mathit{E}$ of
pairwise relations among them, we may be interested in studying the underlying structure
$\mathit{G=(V,E)}$. Such a $\mathit{G}$ is called a \emph{graph}. We say $\mathit{V=V(G)}$
is the set of \emph{vertices} in $\mathit{G}$ and $\mathit{E=E(G)}$ is the set of
\emph{edges} in $\mathit{G}$. An edge $\mathit{e=vw=wv}$ \emph{joins} the vertices
$\mathit{v}$ and $\mathit{w}$. If $\mathit{e=vw}\in\mathit{E}$ then $\mathit{v,w}$ are
\emph{adjacent} to each other and are both \emph{incident} to $\mathit{e}$. As this
description of graphs is fairly general, it can be useful to restrict our thinking to
simpler graphs for a time.

\begin{defn}[Simple Graph] A graph $\mathit{G=(V,E)}$ is said to be \emph{simple} if
\begin{enumerate} \item{$\mathit{V}$ and $\mathit{E}$ are both finite;} \item{For all
$\mathit{vw} \in \mathit{E}, \mathit{v\not=w}$;} \item{$\mathit{E}$ is a set.}
\end{enumerate} \end{defn}

From the above we see that if a graph is simple, then every edge joins two different
vertices. We may like to traverse these edges to ``go'' from one vertex to another.
Indeed, we can form a finite sequence of edges $\mathit{W} = v_{i}v_{i+1}, v_{i+1}v_{i+2},
\dots ,v_{j-1}v_{j} = (v_{i},v_{j})$, a \emph{walk} $\mathit{W}$ from $v_i$ to $v_j$ where
$v_i$ is the \emph{initial vertex} and $v_j$ is the \emph{end vertex}. A walk where every
edge traversed is unique is called a \emph{trail}. Trails where $v_i$ and $v_j$ are the
same are \emph{closed}. As before, we will narrow down the trails we would like to study.

\begin{defn}[Path] A $\mathit{path}$ of length $\mathit{k}$ is a trail where $\mathit{k}$
edges are in the sequence and every vertex visited is unique unless the path is closed. In
other words, if a trail P is such that

\begin{itemize}
  \item{$P = v_{0}v_{1}, v_{1}v_{2}, \dots, v_{k-1}v_{k}$ \enspace and}
  \item{$v_i = v_j\implies\mathit{i = j}, \enspace\forall\mathit{i,j}\in\mathit(0,k)$}
\end{itemize}
then P is a path. In addition, if $v_0 = v_k$ it is a \emph{closed path}.
\end{defn}

\begin{note}
\emph{This formulation of a path is equivalent to some writers' usage of \emph{simple
path}.}
\end{note}

\begin{defn}[Cycle] If a path contains at least one edge and is closed, it is also called
a \emph{cycle}. Conventionally, a cycle of length $\mathit{k}$ is a $\mathit{k}$-cycle.
\end{defn}

If not already considered, observe that the above definition implies the existence of the
trivial path of length zero from a vertex to itself.

\begin{figure}
\centering
\begin{tikzpicture}[node/.style={circle,draw,thick}]

\node[node] at (.5,.5) {};
\node[node] at (.5,-.5) {};
\node[node] at (-.5,-.5) {};
\node[node] at (-.5,.5) {};

\node[node] at (5,.5) (000) {000};
\node[node] (001) [right=of 000] {001};
\node[node] (010) [below=of 000] {010};
\node[node] (011) [right=of 010] {011};
\node[node] (100) [above left=of 000] {100};
\node[node] (101) [above right=of 001] {101};
\node[node] (110) [below left=of 010] {110};
\node[node] (111) [below right=of 011] {111};
\path[-]
  (000) edge (001)
        edge (010)
        edge (100)
  (111) edge (110)
        edge (101)
        edge (011)
  (001) edge (011)
        edge (101)
  (010) edge (110)
        edge (011)
  (100) edge (101)
        edge (110);

\end{tikzpicture}

\caption{On the left is $\mathit{N_4}$ the \emph{null graph} on four vertices which
contains no edges. The next graph is $\mathit{Q_3}$ the \emph{hypercube graph} of
3-dimensions. In graphical representations, edges do not cross vertices. Notice that
$\mathit{V(N_4)} \subseteq \mathit{V(Q_3)}$ and $\mathit{E(N_4)} \subseteq
\mathit{E(Q_3)}$, so $\mathit{N_4}$ is a \emph{subgraph} of $\mathit{Q_3}$ denoted
$\mathit{N_4} \subseteq \mathit{Q_3}$. In particular, because $\mathit{N_4} \not=
\mathit{Q_3}$, $\mathit{N_4}$ is a \emph{proper subgraph} of $\mathit{Q_3}$. Surprising no
one, $\mathit{Q_3}$ is a $\mathit{supergraph}$ of $\mathit{N_4}$.}

\end{figure}

\subsection*{Exercises} \begin{enumerate} \item{(\emph{Bipartite graphs}) A graph
$\mathit{G(V,E)}$ is \emph{bipartite} if $\mathit{V = A} \cup \mathit{B}$ such that
$\mathit{A}$ and $\mathit{B}$ are disjoint and every edge in $\mathit{E}$ is of the form
$\mathit{ab}$ where $\mathit{a} \in \mathit{A}$ and $\mathit{b} \in \mathit{B}$. Show that
$\mathit{G}$ is bipartite if and only if every cycle in $\mathit{G}$ has even length.}

\item{\emph{(Connected graphs, connected components}) Show that the following are
equivalent.} \begin{enumerate}

  \item{Let $\mathit{G}$ be a graph such that for every pair of vertices $\mathit{v,w} \in
  \mathit{V(G)}$, there is a path $\mathit{(v,w)}$ in $\mathit{E(G)}$.}

  \item{A graph $\mathit{G}$ \emph{cannot} be the result of the union of two disjoint
  non-empty and non-identical graphs $\mathit{H}$ and $\mathit{H'}$ (i.e., the 
  intersection of $\mathit{V(H)}$ and $\mathit{V(H')}$ is empty and $\mathit{V(H)} \not=
  \mathit{V(H')}$).}

  \item{Under a partition $\mathit{P}$ on a graph $\mathit{G}$ where for every 
  $\mathit{X}$ in $\mathit{P}$ there is a walk between all vertices in $\mathit{X}$, 
  $\mathit{G = X}$. Every $\mathit{X}$ in such a partition is a 
  \emph{connected component}.}

\end{enumerate}
\end{enumerate}

\end{document}
